// https://ru.stackoverflow.com/q/1354443/195342
// Test for formula for is board NxN solved

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>

using namespace std;

bool invariant(const vector<vector<int>>& b)
{
    int er = 0, ec = 0;
    const int N = b.size();
    const int M = N*N - 1;
    vector<int> a(M);
    for(int k = 0, i = 0; i < N; ++i)
        for(int j = 0; j < N; ++j)
            if (b[i][j]) a[k++] = b[i][j];
            else {
                er = i;
                ec = j;
            }
 
    // for(int i = 0; i < M; ++i) cout << a[i] << " "; cout << endl;
 
    int sum = 0;
    for(int i = 0; i < M - 1; ++i)
        for(int j = i+1; j < M; ++j)
            if (a[i] > a[j]) sum++;
 
    sum += (N-1)*er + 1;
 
    // cout << sum << "   " << er << endl;
    return sum%2 == N%2;
}


void move(vector<vector<int>>& b, int& er, int& ec, int N)
{
    int olr = er, olc = ec;
    for(bool ok = false;!ok;)
    {
        switch(rand()%4)
        {
        case 0: if (er > 0)   { er--; ok = true; } break;
        case 1: if (ec < N-1) { ec++; ok = true; } break;
        case 2: if (er < N-1) { er++; ok = true; } break;
        case 3: if (ec > 0)   { ec--; ok = true; } break;
        }
    }
    // cout << er << " <> " << ec << endl;
    swap(b[olr][olc],b[er][ec]);
}

template<typename INV>
bool Experiment(int N, INV inv, int count = 1000000)
{
    vector<vector<int>> b(N,vector<int>(N));
    for(int k = 0, i = 0; i < N; ++i)
        for(int j = 0; j < N; ++j) b[i][j] = ++k;
    b[N-1][N-1] = 0;
    int er = N-1, ec = N-1;

    for(int i = 0; i < count; ++i)
    {
        move(b,er,ec,N);
        if (!inv(b)) return false;
    }

    for(int k = 0, i = 0; i < N; ++i)
        for(int j = 0; j < N; ++j) b[i][j] = ++k;
    b[N-1][N-1] = 0;
    swap(b[N-1][N-2],b[N-1][N-3]);

    er = N-1; ec = N-1;

    for(int i = 0; i < count; ++i)
    {
        move(b,er,ec,N);
        if (inv(b)) return false;
    }

    return true;
}

int main(int argc, char * argv[])
{
    srand(time(0));
    for(int n = 3; n < 40; ++n)
    {
        cout << "n = " << n << " ---> "
            << (Experiment(n,invariant) ? "ok\n" : "fail\n");
    }
}

