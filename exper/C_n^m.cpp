// https://ru.stackoverflow.com/a/499329/195342
// Метод вычисления биномиального коэффициента как сократимой дроби
// и исследование его применимости (до 63 включительно)

#include <iostream>
#include <iomanip>
#include <assert.h>
#include <numeric>

#include <boost/multiprecision/cpp_int.hpp>
using large = boost::multiprecision::cpp_int;

using namespace std;

unsigned long long C(unsigned long long n, unsigned long long m)
{
    unsigned long long q = 1, d = 1;
    for(; m < n;)
    {
        d *=(n-m);
        q *= ++m;
        unsigned long long z = gcd(q,d);
        q/= z; d /= z;
    }
    if (d != 1)
    {
        cerr << "error at n = " << n << endl;
    }
    return q;
}

large factorial(large n)
{
    large f = 1;
    if (n <= 1) return f;
    for(int k = 2; k <= n; ++k) f*= k;
    return f;
}

large C(large n, large m)
{
    return factorial(n)/factorial(m)/factorial(n-m);
}

int main(int argc, const char * argv[])
{
    for(int N = 10; ; ++N)
    {
        for(int M = 1; M <= N/2; ++M)
        {
            if (C(N,M) != C(large(N),large(M)))
            {
                cout << N << " : " << M << endl;
                return 0;
            }
        }
    }
}


