#include "tester.h"

void DLL_SPEC Tester::test()
{
    std::cout << "Tester::test() : ";
    Calcus c(2);
    c.out();
    std::cout << "testValue = " << testValue++ << "\n";
}

void DLL_SPEC Tester::test(Calcus&c)
{
    static Calcus v(10000);
    v.out();

    std::cout << "Tester::test(Calcus&c) : ";
    c.out();
}
void DLL_SPEC Tester::test(Calcus&&c)
{
    std::cout << "Tester::test(Calcus&&c) : ";
    c.out();
}

int DLL_SPEC Tester::testValue = 555;




