#include "tester.h"

namespace Tester {

    Calcus::Calcus(int i):i_(i) { std::cout << "Calcus::Calcus("<<i_<<")\n"; }
    Calcus::Calcus(const Calcus&  c):i_(c.i_) { std::cout << "Calcus::Calcus(Calcus&("<<i_<<"))\n"; }
    Calcus::Calcus(const Calcus&& c):i_(c.i_) { std::cout << "Calcus::Calcus(Calcus&&("<<i_<<"))\n"; }
    Calcus::~Calcus() { std::cout << "Calcus::~Calcus("<<i_<<")\n"; }
    void Calcus::out() const { std::cout << "Calcus::out("<<i_<<")\n"; }


    Calcus v__(1000000);

}

