#if !defined(TESTER_H__)
#define      TESTER_H__

#include <string>
#include <iostream>

#if defined(MAKE_DLL__)
#define  DLL_SPEC  __declspec(dllexport)
#else
#define  DLL_SPEC  __declspec(dllimport)
#pragma comment(lib,"Sester")
#endif


namespace Tester {

    class DLL_SPEC Calcus
    {
    public:
        Calcus(int i);
        Calcus(const Calcus&  c);
        Calcus(const Calcus&& c);
        virtual ~Calcus();
        virtual void out() const;
    private:
        int i_;
    };

    void DLL_SPEC test();
    void DLL_SPEC test(Calcus&c);
    void DLL_SPEC test(Calcus&&c);

    extern int DLL_SPEC testValue;

}



#endif

