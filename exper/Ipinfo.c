// ���������� �� IP-������

#include <stdio.h>

unsigned long addr, mask, bits, net, x;

void usage(char * s)
{
    fprintf(stderr,"%s"
            "Usage: Ipinfo  ###.###.###.###/##\n"
            "   or: Ipinfo  ###.###.###.### ###.###.###.###\n",s);
}

char * msgan = "Wrong address/netmask\n";
char * msga  = "Wrong address\n";
char * msgn  = "Wrong netmask\n";

int main(int argc, char*argv[])
{
    int o1,o2,o3,o4,o5;
    if (argc == 2)
    {
        if(sscanf(argv[1],"%d.%d.%d.%d/%d",&o1,&o2,&o3,&o4,&o5) != 5)
        {
            usage(msgan); return 1;
        }
        if ((o1 < 0) || (o1 > 255) ||
            (o2 < 0) || (o2 > 255) ||
            (o3 < 0) || (o3 > 255) ||
            (o4 < 0) || (o4 > 255) ||
            (o5 <= 0) || (o5 > 31))
        {
            usage(msgan); return 1;
        }
        addr = (o1 << 24) + (o2 << 16) + (o3 << 8) + o4;
        mask = (0xFFFFFFFF << (32 - (bits = o5)));
    } else if (argc == 3)
    {
        if(sscanf(argv[1],"%d.%d.%d.%d",&o1,&o2,&o3,&o4) != 4)
        {
            usage(msga); return 1;
        }
        if ((o1 < 0) || (o1 > 255) ||
            (o2 < 0) || (o2 > 255) ||
            (o3 < 0) || (o3 > 255) ||
            (o4 < 0) || (o4 > 255))
        {
            usage(msga); return 1;
        }
        addr = (o1 << 24) + (o2 << 16) + (o3 << 8) + o4;
        if(sscanf(argv[2],"%d.%d.%d.%d",&o1,&o2,&o3,&o4) != 4)
        {
            usage(msgn); return 1;
        }
        if ((o1 < 0) || (o1 > 255) ||
            (o2 < 0) || (o2 > 255) ||
            (o3 < 0) || (o3 > 255) ||
            (o4 < 0) || (o4 > 255))
        {
            usage(msgn); return 1;
        }
        mask = (o1 << 24) + (o2 << 16) + (o3 << 8) + o4;
        x = ~mask;
        if ((x&(x+1)) != 0)
        {
            usage(msgn); return 1;
        }
        for(bits = 0; x; ++bits) x >>= 1;
        bits = 32 - bits;
    } else
    {
        usage(""); return 1;
    }

    net = addr&mask;

    printf("IP address      %3d  .   %3d  .   %3d  .   %3d  / %2d     %d.%d.%d.%d/%d\n",
           addr >> 24, (addr >> 16)&0xFF, (addr >> 8)&0xFF, addr&0xFF, bits,
           addr >> 24, (addr >> 16)&0xFF, (addr >> 8)&0xFF, addr&0xFF, bits);
    printf("IP bits      %08b.%08b.%08b.%08b\n",
           addr >> 24, (addr >> 16)&0xFF, (addr >> 8)&0xFF, addr&0xFF);
    printf("Mask bytes      %3d  .   %3d  .   %3d  .   %3d  / %2d     %d.%d.%d.%d\n",
           mask >> 24, (mask >> 16)&0xFF, (mask >> 8)&0xFF, mask&0xFF, bits,
           mask >> 24, (mask >> 16)&0xFF, (mask >> 8)&0xFF, mask&0xFF, bits);
    printf("Mask bits    %08b.%08b.%08b.%08b\n",
           mask >> 24, (mask >> 16)&0xFF, (mask >> 8)&0xFF, mask&0xFF);
    printf("Network         %3d  .   %3d  .   %3d  .   %3d           %d.%d.%d.%d\n",
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF,
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF);
    net = net|~mask;
    printf("Broadcast       %3d  .   %3d  .   %3d  .   %3d           %d.%d.%d.%d\n",
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF,
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF);
    net = (addr&mask) + 1;
    printf("First host      %3d  .   %3d  .   %3d  .   %3d           %d.%d.%d.%d\n",
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF,
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF);
    net = addr&mask;
    net |= ~mask;
    --net;
    printf("Last host       %3d  .   %3d  .   %3d  .   %3d           %d.%d.%d.%d\n",
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF,
           net >> 24, (net >> 16)&0xFF, (net >> 8)&0xFF, net&0xFF);
    printf("Total hosts     %d\n", ~mask - 1);


    return 0;
}

