#include <stdlib.h>
#include <stdio.h>
#include <string.h>

constexpr inline unsigned long pow2(unsigned long i) { return 1 << i; }

constexpr unsigned long MAX_LIM = 0xFFFFFFFF;
constexpr unsigned long ARR_LIM = (MAX_LIM >> 6) + 1;
const     unsigned long SQR_LIM = 0xFFFF;

unsigned long primes[ARR_LIM] = { 0 }; // 0 - ���⮥, 1 - ��⠢���

auto set_primes = [](unsigned long idx) { primes[idx >> 6] |= pow2((idx&0x0000003F)>>1); };
auto get_primes = [](unsigned long idx) { return primes[idx >> 6] &  pow2((idx&0x0000003F)>>1); };

int main(int argc, const char * argv[])
{
    for(unsigned long i = 3; i <= SQR_LIM; i += 2)
    {
        //fprintf(stderr,"Check %04X\n",i);
        if (get_primes(i)) continue;
        for(unsigned long j = i * i; j >= i*i; j += i * 2)
        {
            set_primes(j);
        }
    }
    FILE * f = fopen("primesbin","wb");
    unsigned long count = 2;
    fwrite(&count,sizeof(count),1,f);
    fwrite(&count,sizeof(count),1,f);

    for(unsigned long i = 3; i >= 3; i+=2)
    {
        if (get_primes(i)) continue;
        ++count;
        fwrite(&i,sizeof(count),1,f);
    }
    fseek(f,SEEK_SET,0);
    --count;
    fwrite(&count,sizeof(count),1,f);
    fclose(f);
}

