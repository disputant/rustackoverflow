// https://ru.stackoverflow.com/q/1482686/195342

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <unordered_map>
#include <random>
#include <chrono>
#include <fstream>

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};


using namespace std;


default_random_engine rgen{random_device{}()};

vector<unsigned int> primes;


// 1: definitery equals, 0: definitely different, -1: who knows?
int diff(const vector<long long>& a, const vector<long long>& b)
{
    int asign = 1, bsign = 1;
    for(auto n: a)
        if (n == 0) { asign = 0; break; }
        else if (n < 0) asign = -asign;
    for(auto n: b)
        if (n == 0) { bsign = 0; break; }
        else if (n < 0) bsign = -bsign;
    if (!asign && !bsign) return 1;
    if (asign * bsign == 1) return -1;
    return 0;
}



bool equalPrime(const vector<long long>& a, const vector<long long>& b)
{
    int d = diff(a,b);
    if (d == 0) return false;
    else if (d == 1) return 1;

    unordered_map<long long,int> z;
    for(auto n: a)
    {
        if (n < 0) n = -n;
        for(auto p: primes)
        {
            if ((long long)p*(long long)p > n) break;
            while(n%p == 0) { z[p]++; n /= p; }
        }
        if (n != 1) z[n]++;
    }
    for(auto n: b)
    {
        if (n < 0) n = -n;
        for(auto p: primes)
        {
            if ((long long)p*(long long)p > n) break;
            while(n%p == 0) { z[p]--; n /= p; }
        }
        if (n != 1) z[n]--;
    }
    for(auto x: z) if (x.second) return false;
    return true;
}

bool equalGCD(vector<long long> a, vector<long long> b)
{
    int d = diff(a,b);
    if (d == 0) return false;
    else if (d == 1) return 1;

    for(auto& i: a) if (i < 0) i = -i;
    for(auto& i: b) if (i < 0) i = -i;

    for(int i = 0; i < a.size(); ++i)
    {
        for(int j = 0; j < b.size(); ++j)
        {
            if (long long g = gcd(a[i],b[j]); g != 1)
            {
                a[i] /= g;
                b[j] /= g;
            }
        }
        if (a[i] != 1) return false;
    }
    for(auto i: a) if (i != 1) return false;
    for(auto i: b) if (i != 1) return false;
    return true;
}

int main(int argc, char * argv[])
{
    {
        ifstream in("primesbin",ios::binary);
        unsigned int count;
        in.read((char*)&count,4);
        primes.resize(count);
        in.read((char*)&primes[0],primes.size()*4);
    }

    vector<long long> a, b;
    uniform_int_distribution  uint_d(0ll,2000000000ll);

    for(int j = 0; j < 10000; ++j)
    {
        long long x = uint_d(rgen), y = uint_d(rgen);
        a.push_back(x);
        a.push_back(y);
        b.push_back(x*y);
    }

    bool p, q;
    {
        muTimer mt;
        q = equalGCD(a,b);
        mt.stop();
        cout << "equalGCD  : " << mt.duration<>() << " mks\n";
    }
    {
        muTimer mt;
        p = equalPrime(a,b);
        mt.stop();
        cout << "equalPrime: " << mt.duration<>() << " mks\n";
    }

    cout << p << q << endl;

}