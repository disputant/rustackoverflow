#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <map>
#include <chrono>

using namespace std;

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};


// Создание всех возможных расстановок с помощью симметрий
vector<int> makeV(const vector<int>& a)
{
    const size_t N = a.size();
    vector<vector<int>> p(8,vector<int>(N));
    for(int i = 0; i < N; ++i)
    {
        p[0][i]        = int(a[i]);
        p[1][i]        = int(N-1-a[i]);// H
        p[2][N-1-i]    = int(a[i]);    // V
        p[3][N-1-i]    = int(N-1-a[i]);// O
        p[4][a[i]]     = int(i);       // \s
        p[5][a[i]]     = int(N-1-i);   // ->
        p[6][N-1-a[i]] = int(i);       // <-
        p[7][N-1-a[i]] = int(N-1-i);   // /s
    }
    return *min_element(p.begin(),p.end());
}

// Класс для сравнения двух векторов (по сути, можно ли получить один
// из второго симметриями
struct lessVec{

    bool operator()(const vector<int>& a, const vector<int>& b) const
    {
        vector<int> p = makeV(a);
        vector<int> q = makeV(b);

        return p < q;
    }
};

// true, если есть пара бьющих ферзей
bool fight(const vector<int>& p, unsigned int&i, unsigned int&j)
{
    for(i = 0; i < p.size()-1; ++i)
    {
        for(j = i+1; j < p.size(); ++j)
        {
            if (int(j-i) == abs(p[j]-p[i])) return true;
        }
    }
    return false;
}

// Решение для доски NxN
pair<int,int> solve(int N)
{
    map<vector<int>,int,lessVec> m; // Собираю группы решений

    // Проверяю все перестановки
    vector<int> p(N);
    for(int i = 0; i < N; ++i) p[i] = i;

    int count = 0;  // Количество решений
    do {
        unsigned int i = 0, j = 0;
        if (!fight(p,i,j))
        {
            ++count;
            m[p]++;    // Сколько в группе решений
        }
        else
        {
            if (j) sort(p.begin()+j+1,p.end(),greater<int>());
        }
    } while(next_permutation(p.begin(),p.end()));

    return make_pair(count,int(m.size()));
}

int main()
{
    for(int i = 1; i <= 15; ++i)
    {
        muTimer mt;
        auto p = solve(i);
        mt.stop();
        cout << setw(2) << i << "  "
            << setw(7) << p.first << "  "
            << setw(7) << p.second << "   " << mt.duration<muTimer::ms>() << " ms" << endl;
        ofstream out("res",ios::ate|ios::app);
        out << setw(2) << i << "  "
            << setw(7) << p.first << "  "
            << setw(7) << p.second << "   " << mt.duration<muTimer::ms>() << " ms" <<  endl;
    }
}

