    // ������ ��� �� �����
    class Random
    {
    public:
        typedef int RandomValue;
        Random& operator = (int seed) { X = seed; return *this; }
        Random(int seed = 1):X(seed){};
        int operator()(int seed = 0)
        {
            const int MM = 2147483647;
            const int AA =      48271;
            const int QQ =      44488;
            const int RR =       3399;
            if (seed != 0) X = seed;
            X = AA*(X%QQ)-RR*(X/QQ);
            if (X < 0) X += MM;
            return X-1;
        }
        // �� ������� max
        int operator()(int min, int max)
        {
            return (*this)()%(max-min) + min;
        }
    private:
        int X;
    };

    class Random64
    {
    typedef unsigned long long uint64;
    public:
        typedef uint64 RandomValue;
        Random64& operator = (uint64 seed) { X = seed; return *this; }
        Random64(uint64 seed = 0):X(seed){};
        uint64 operator()(uint64 seed = uint64(-1))
        {
            const uint64 a = 3202034522624059733ULL;
            const uint64 c =                   1ULL;
    
            if (seed != uint64(-1)) X = seed;
            uint64 Y = a * X + c;
            X = a * Y + c;
            Y = (Y&0xFFFFFFFF00000000ULL) | (X >> 32);
            return Y;
        }
        // �� ������� max
        uint64 operator()(uint64 min, uint64 max)
        {
            return (*this)()%(max-min) + min;
        }
    private:
        uint64 X;
    };
