// https://ru.stackoverflow.com/q/1421676/195342

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <numeric>
#include <execution>
#include <random>
using namespace std;

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};

size_t arr_dist(size_t n, const char * a, const char * b) {
  size_t result = 0;
  for (size_t i = 0; i < n; i++)
    result += a[i] != b[i];
  return result;
}

int main(int argc, const char * argv[])
{
    const int SIZE = 1000000000;

    char * a = new char[SIZE];
    char * b = new char[SIZE];

    default_random_engine e(random_device{}());
    uniform_int_distribution<int> dist(0,127);

    for(int i = 0; i < SIZE; ++i)
    {
        a[i] = dist(e);
        b[i] = dist(e);
    }
    {
        muTimer mt;
        size_t sz = arr_dist(SIZE,a,b);
        mt.stop();
        printf("%zd %lld ms\n",sz,mt.duration<muTimer::mks>());
    }
    {
        muTimer mt;
        size_t sz = inner_product(a,a+SIZE,b,0,plus<>(),[](char x, char y){ return x != y; });
        mt.stop();
        printf("%zd %lld ms\n",sz,mt.duration<muTimer::mks>());
    }
    {
        muTimer mt;
        size_t sz = transform_reduce(execution::par,a,a+SIZE,b,0,plus<>(),[](char x, char y){ return x != y; });
        mt.stop();
        printf("%zd %lld ms\n",sz,mt.duration<muTimer::mks>());
    }
    {
        muTimer mt;
        size_t sz = transform_reduce(execution::par_unseq,a,a+SIZE,b,0,plus<>(),[](char x, char y){ return x != y; });
        mt.stop();
        printf("%zd %lld ms\n",sz,mt.duration<muTimer::mks>());
    }

}

