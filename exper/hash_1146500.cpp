// https://ru.stackoverflow.com/q/1146500/195342
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <random>
#include <set>

using namespace std;

using word = unsigned int;

vector<string> vs;  //  Строки
set<string> un;     //  Реально уникальные строки
set<word> has;      // уникальные хеши

word Hash(const string& s)
{
    word z = hash<string>{}(s); // &0xFFFFFF; // <- for 3 bytes hash
    return z;
}

int main()
{
    for(int i = 0; i <= 500000; ++i)     // столько разных строк
    {
        string s;
        for(int j = rand()%8+3; j >= 0; --j)  // Строки до 10 символов длиной
            s += (char)(rand()%26+'a');       
        for(int j = rand()%20; j >= 0; --j)   // Их дублирование (в среднем - 10)
            vs.push_back(s);
    }
    shuffle(vs.begin(),vs.end(), default_random_engine(random_device()())); // Перемешали

    for(int i = 0; i < vs.size(); ++i)   // По всем случаям 
    {
        un.insert(vs[i]);        // Подсчет уникальных
        word h = Hash(vs[i]);    // Хеш
        has.insert(h);           // Подсчет уникальных хешей

        if (i%10000 == 0) 
            cout << setw(10) << i << setw(10) 
                 << un.size()                    //  Уникальных строк
                 << setw(10) << has.size()       // Уникальных хешей
                 << "   " << (double(has.size()) - un.size())*100.0/un.size() << "\n"; // Разница в %
    }
}
