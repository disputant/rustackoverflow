//https://ru.stackoverflow.com/q/1276785/195342
#include <vector>
#include <iostream>
#include <iomanip>
#include <regex>
#include <chrono>
#include <random>

using namespace std;

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};

regex rg("-{0,1}\\d{11}");

inline int match_r(const char * s)
{
    return regex_match(s,rg);
}

inline int match_p(const char * s)
{
    if (*s == '-') ++s;
    for(int i = 0; i < 11 ; ++i, ++s)
        if (*s > '9' || *s < '0') return 0;
    return *s == 0;
}

int main(int argc, char * argv[])
{
    vector<char*> v;
    random_device r;
    default_random_engine e(r());
    uniform_int_distribution<long long> dist(10000000000,99999999999);
    const int N = 1'000'000;
    for(int i = 0; i < N; ++i)
    {
        char * s = new char[20];
        long long L = dist(e);
        if (i%2) L = -L;
        sprintf(s,"%lld",L);
        v.push_back(s);
        s = new char[20];
        sprintf(s,"%lld",L);
        switch(N%3)
        {
        case 0: s[dist(e)%8+2] = 0; break;
        case 1: strcat(s,"123"); break;
        case 2: s[dist(e)%8+2] = 'a' + dist(e)%8+2; break;
        }
        v.push_back(s);
    }

    {
        muTimer mt;
        int cnt = 0;
        for(auto s: v) cnt += match_r(s);
        mt.stop();
        cout << cnt << " for " << mt.duration<>() << " mks\n";
    }
    {
        muTimer mt;
        int cnt = 0;
        for(auto s: v) cnt += match_p(s);
        mt.stop();
        cout << cnt << " for " << mt.duration<>() << " mks\n";
    }

}
