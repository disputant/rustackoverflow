// https://ru.stackoverflow.com/q/1148546/195342

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <chrono>

template<
    typename Iter,
    typename = std::enable_if_t<std::is_arithmetic<std::iterator_traits<Iter>::value_type>::value>
    >
    auto average(Iter b, Iter e)
{
    using Double = std::common_type_t<typename std::iterator_traits<Iter>::value_type,double>;
    Double
        x  = Double{0},
        x2 = Double{0};
    size_t N = 0;
    for(;b!=e;++b)
    {
        x  += static_cast<Double>(*b);
        x2 += static_cast<Double>(*b)*static_cast<Double>(*b);
        ++N;
    }
    x2 = sqrt((N*x2-x*x)/N/(N-1));
    x  = x/N;
    return std::pair<Double,Double>(x,x2);
};

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};

using namespace std;


inline void add_string1(vector<string>& strings,std::string str) {
    strings.push_back(std::move(str));
}

inline void add_string2(vector<string>& strings,std::string str) {
    strings.emplace_back(std::move(str));
}

inline void add_string3(vector<string>& strings,const std::string& str) {
    strings.push_back(str);
}

inline void add_string4(vector<string>& strings, const std::string& str) {
    strings.emplace_back(str);
}

inline void add_string5(vector<string>& strings, std::string_view str) {
    strings.emplace_back(str.data());
}

int main()
{
    const int Count = 1000000, eCount = 20;
    vector<string> rnd;
    for(int i = 0; i < Count; ++i)
    {
        string s;
        for(int j = rand()%50+10; j --> 0; )
            s += 'a' + rand()%26;
        rnd.push_back(s);
    }

    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z;
            for(int i = 0; i < Count; ++i) add_string1(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z;
            for(int i = 0; i < Count; ++i) add_string2(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z;
            for(int i = 0; i < Count; ++i) add_string3(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z;
            for(int i = 0; i < Count; ++i) add_string4(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z;
            for(int i = 0; i < Count; ++i) add_string5(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }

    // Reserve memory

    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z; z.reserve(Count);
            for(int i = 0; i < Count; ++i) add_string1(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z; z.reserve(Count);
            for(int i = 0; i < Count; ++i) add_string2(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z; z.reserve(Count);
            for(int i = 0; i < Count; ++i) add_string3(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z; z.reserve(Count);
            for(int i = 0; i < Count; ++i) add_string4(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }
    {
        vector<long long> ts;
        for(int j = 0; j < eCount; ++j)
        {
            muTimer mt;
            vector<string> z; z.reserve(Count);
            for(int i = 0; i < Count; ++i) add_string5(z,rnd[i]);
            mt.stop();
            ts.push_back(mt.duration<muTimer::ms>());
        }
        auto [x,s] = average(ts.begin(),ts.end());
        cout  << setprecision(1) << fixed << setw(6) << x << " +- " << setprecision(1) << s << " ms\n";
    }


}
