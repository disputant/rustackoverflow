#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <random>

using namespace std;


double Xi[20][2] = // For \alpha = 0.10 && 0.05
{
    {  2.7,  3.8 },         // 1
    {  4.6,  5.9 },
    {  6.3,  7.8 },
    {  7.8,  9.5 },
    {  9.2, 11.1 },
    { 10.6, 12.6 },
    { 12.0, 14.1 },
    { 13.4, 15.5 },
    { 14.7, 16.9 },
    { 16.0, 18.3 },
    { 17.3, 19.7 },
    { 18.5, 21.0 },
    { 19.8, 22.4 },
    { 21.1, 23.7 },
    { 22.3, 25.0 },
    { 23.5, 26.3 },
    { 24.8, 27.6 },
    { 26.0, 28.9 },
    { 27.2, 30.1 },
    { 40.3, 43.8 }   // > 30...
};


default_random_engine g(random_device{}());


void Experiment(int N, int Count = 10000)
{
    uniform_int_distribution<> dis(0, N-1);
    vector<int> r(N), u(N);
    for(int i = 0; i < Count; ++i)
    {
        r[rand()%N]++;
        u[dis(g)]++;
    }
    double rs = 0, us = 0;
    for(int i = 0; i < N; ++i)
    {
        double d = double(r[i])/Count - 1./N;
        rs += d*d;
        d = double(u[i])/Count - 1./N;
        us += d*d;
    }

    rs = rs * Count * N;
    us = us * Count * N;

    double xi = (N < 20) ? Xi[N-2][1] : Xi[19][1];


    cout << setw(2) << N << "   " << fixed << setprecision(1)
        << setw(5) << xi << "   " << setprecision(2)
        << setw(7) << rs << ( rs < xi ? "  ok  " : "  fail")
        << "     "
        << setw(7) << us << ( us < xi ? "  ok  " : "  fail")
        << endl;
}



int main(int argc, char * argv[])
{
    srand(time(0));
    for (int Count = 10; Count <= 100000; Count *= 10)
    {
        cout << "\n\nValues count = " << Count << "\n";
        cout << " N  xi(0.05)    rand()%N          uniform\n";
        cout << "-----------------------------------------\n";
        for(int N = 2; N < 10; ++N) Experiment(N,Count);
    }

}




