#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdio>
#include <chrono>

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};

using namespace std;

int main(int argc, const char * argv[])
{
    const int count = 10000;

    long long cout_endl, cout_n, cout_untied, con_file, printf_con;
    // First, test cout with endl
    {
        muTimer mu;
        for(int i = 0; i < count; ++i)
        {
            cout << i << endl;
        }
        mu.stop();
        cout_endl = mu.duration<>();
    }
    // Second, test cout with "\n"
    {
        muTimer mu;
        for(int i = 0; i < count; ++i)
        {
            cout << i << "\n";
        }
        cout << endl;
        mu.stop();
        cout_n = mu.duration<>();
    }
    // Third, test "untied" cout
    {
        cout.sync_with_stdio(false);
        cin.tie(nullptr);
        muTimer mu;
        for(int i = 0; i < count; ++i)
        {
            cout << i << "\n";
        }
        cout << endl;
        mu.stop();
        cout_untied = mu.duration<>();
    }
    // Fourth, test file "con" out
    {
        ofstream cout("con");
        muTimer mu;
        for(int i = 0; i < count; ++i)
        {
            cout << i << "\n";
        }
        cout << endl;
        mu.stop();
        con_file = mu.duration<>();
    }
    // Fifth, test printf
    {
        muTimer mu;
        for(int i = 0; i < count; ++i)
        {
            printf("%d\n",i);
        }
        fflush(stdout);
        mu.stop();
        printf_con = mu.duration<>();
    }

    cout << "-------- 8< --------\n";
    cout << " cout with endl | cout with \\n | cout \"untied\" | file \"con\" | printf\n";
    cout << setw(12) << cout_endl/1000   << " ms | "
         << setw( 9) << cout_n/1000      << " ms | "
         << setw(10) << cout_untied/1000 << " ms | "
         << setw( 7) << con_file/1000    << " ms | "
         << setw( 4) << printf_con/1000  << " ms\n";

    getchar();

}

