// https://www.e-olymp.com/ru/problems/2328
#include <iostream>
#include <cmath>

using namespace std;

int qpow(int x, unsigned int e)
{
    int res = 1;
    for(;e;e>>=1)
    {
        if (e&1) res *= x;
        x *= x;
    }
    return res;
}

unsigned int pp[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 };

bool isPow(int n)
{
    double x = log(n);
    for(auto p: pp)
    {
        int a = int(exp(x/p)+0.5);
        if (qpow(a,p) == n) return true;
    }
    return false;
}

int main()
{
    int N;
    cin >> N;
    for(int i = 0; i < N; ++i)
    {
        int n;
        cin >> n;
        cout << (isPow(n) ? "YES" : "NO") << endl;
    }
}
