// https://www.eolymp.com/ru/problems/3922
// ������ ���� � ����� � ��������� ������� � ����, �� ������� ������ 
// ��������� �������� �������. ����� ���� ������������ ������� ����� �����.

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>

using namespace std;

struct Msg
{
    int no;
    string subj;
    string text;
    int    count = 0;
};

int main()
{
    int n;
    cin >> n;
    vector<Msg> m;
    for(int i = 0; i < n; ++i)
    {
        Msg s;
        cin >> s.no; cin.ignore(1000,'\n');
        if (s.no == 0) getline(cin,s.subj);
        getline(cin,s.text);
        m.push_back(s);
    }
    for(size_t i = m.size(); i-->0; )
    {
        m[i].count++;
        if (m[i].no) m[m[i].no-1].count += m[i].count;
    }

    //for(auto x: m)
    //{
    //    cout << x.no << " " << x.count << "  " << x.text << endl;
    //}

    auto it = *max_element(m.begin(),m.end(),
                           [](const Msg& a, const Msg& b)
                           {
                               if (a.no > b.no) return true;
                               if (a.no < b.no) return false;
                               return a.count < b.count;
                           });
    cout << it.subj;
}
