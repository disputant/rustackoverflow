//https://www.e-olymp.com/ru/problems/573
#include <iostream>
#include <iomanip>

using namespace std;


int main(int argc, const char * argv[])
{
    int N;
    cin >> N;
    for(int r = 0, n = 0; n <= N; ++n)
    {
        r = (r*10+1)%N;
        if (r == 0)
        {
            for(int i = 0; i <= n; ++i) cout << 1;
            cout << endl;
            return 0;
        }
    }
    cout << "no\n";
}
