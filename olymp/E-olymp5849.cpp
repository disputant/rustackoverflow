#include <iostream>
#include <iomanip>

//https://www.e-olymp.com/ru/problems/5849

using namespace std;

long long int m[100001];
long long int h[100001];

long long int sq(long long int n) { return n*n; }
long long int min(long long int x, long long int y) { return (x < y) ? x : y; }

int main()
{
    int n;
    cin >> n;
    for(int i = 1; i <= n; ++i)
    {
        cin >> h[i];
    }

    m[1] = 0;
    m[2] = sq(h[2]-h[1]);

    if (n==2) { cout << m[2] << endl; return 0; }

    m[2] = min(m[2], 3*sq(h[1]-h[3]) + sq(h[2]-h[3]));

    for(int i = 3; i <= n; ++i)
    {
        m[i] = min(m[i-1] + sq(h[i-1]  -h[i]),
                   m[i-2] + 3*sq(h[i-2]-h[i]));

        if (i < n) m[i] = min(m[i], m[i-1] + 3*sq(h[i-1]-h[i+1]) + sq(h[i]-h[i+1]));
    }
    cout << m[n] << endl;
}
