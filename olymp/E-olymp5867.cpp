#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

//https://www.e-olymp.com/ru/problems/5867

using namespace std;

int mi[1000001];

int main(int argc, const char * argv[])
{
    int n,m;
    cin >> n;
    for(int i = 0; i <= n; ++i) mi[i] = n+1;

    for(int i = 1; i*i*i <= n; ++i)
    {
        m = i;
        mi[i*i*i] = 1;
    }
    while(mi[n] > n)
    {
        //  � ������� ��� �ਡ���塞 ��।��� ��...
        for(int i = 1; i <= m; ++i)
        {
            int t = i*i*i;
            for(int j = 1; j+t <= n; j++)
            {
                if (mi[j]+1 < mi[j+t]) mi[j+t] = mi[j]+1;
            }
        }
    }
    cout << mi[n] << endl;
}
