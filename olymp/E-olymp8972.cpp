// https://www.e-olymp.com/ru/problems/8972
#include <iostream>
#include <algorithm>

using namespace std;

struct item
{
    int count, value, idx;
    item():value(-1),count(0),idx(-1){}
};

int main()
{
    item x[201] = {};
    for(int i = 0; i < 201; ++i) x[i].value = i-100;

    int N;
    cin >> N;
    for(int d, i = 0; i < N; ++i)
    {
        cin >> d;
        if (x[d+100].idx < 0) x[d+100].idx = i;
        x[d+100].count++;
    }

    sort(x,x+201,[](const item& a, const item& b){ return a.idx < b.idx;});

    bool has = false;
    for(int i = 0; i < 201; ++i)
        if (x[i].count > 1) { has = true; cout << x[i].value << " "; }

    if (!has) cout << "NO";
    cout << endl;

}
