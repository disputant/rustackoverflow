/* https://acmp.ru/index.asp?main=task&id_task=421
 */

#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

int sq(int x) { return x*x; }

int prod(int x, int y, int x0, int y0, int x1, int y1)
{
    if (sq(x0-x)+sq(y0-y) > sq(x1-x)+sq(y1-y))
    {
        int t = x0; x0 = x1; x1 = t;
        t = y0; y0 = y1; y1 = t;
    }
    return (x0-x)*(y1-y)-(x1-x)*(y0-y);
}

vector<int> getTri()
{
    int x1, y1, x2, y2, x3, y3;
    cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
    vector<int> t;
    int a, b, c;
    t.push_back(a = sq(x1-x2)+sq(y1-y2));
    t.push_back(b = sq(x2-x3)+sq(y2-y3));
    t.push_back(c = sq(x3-x1)+sq(y3-y1));
    sort(t.begin(),t.end());
    if (t[0] == t[1]) t.push_back(0);
    else
    {
        if (t[2] == a) t.push_back(prod(x3,y3,x2,y2,x1,y1));
        if (t[2] == b) t.push_back(prod(x1,y1,x2,y2,x3,y3));
        if (t[2] == c) t.push_back(prod(x2,y2,x3,y3,x1,y1));
    }
    return t;
}

int main(int argc, char * argv[])
{
    int N;
    cin >> N;
    vector<vector<int>> ts;
    for(int i = 0; i < N; ++i)
    {
        ts.push_back(getTri());
    }
    for(int i = 1; i < N; ++i)
    {
        if (ts[i] != ts[0]) { cout << "NO\n"; return 0; }
    }

    cout << "YES\n";
}
