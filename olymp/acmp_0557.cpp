// https://acmp.ru/index.asp?main=task&id_task=557
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <limits>

using namespace std;


int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int m, n, a, b, p;
    cin >> m >> n >> a >> b >> p;
    --a; --b;
    vector<int> A(n), C(n);

    cin.ignore(100000,'\n');
    cin.ignore(100000,'\n');

    for(int i = 0; i < n; ++i)
    {
        if (i == a)
            for(int j = 0; j < n; ++j) { cin >> A[j]; /* cout << A[j] << endl; */ }
        cin.ignore(100000,'\n');
    }

    for(int k = 1; k < m; ++k)
    {
        C = vector<int>(n);
        for(int l = 0; l < n; ++l)
        {
            for(int j = 0; j < n; ++j)
            {
                int x;
                cin >> x; /* cout << "x = " << x << endl; */
                C[j] += A[l]*x;
            }
        }
        for(int l = 0; l < n; ++l)
            A[l] = C[l]%p;
    }
    cout << A[b] << endl;
}
