// https://acmp.ru/asp/do/index.asp?main=task&id_course=1&id_section=4&id_topic=40&id_problem=707
#include <vector>
#include <string>
#include <iostream>

using namespace std;

string to3(int n)
{
    string r;
    while(n)
    {
        r += '0'+ n%3;
        n /= 3;
    }
    int carry = 0;
    for(int i = 0; i < r.size(); ++i)
    {
        int m = r[i]-'0'+carry;
        carry = (m >= 2);
        r[i] = (m <= 1) ? '0' + m :
            (m == 2) ? 'i' : '0';
    }
    if (carry) r += '1';
    return r;
}


int main()
{
    char C; int N;
    cin >> C >> N;
    string s = to3(N);
    int w = 1;
    vector<int> L, R;
    for(char c: s)
    {
        switch (c)
        {
        case '0': break;
        case '1': L.push_back(w); break;
        case 'i': R.push_back(w); break;
        }
        w *= 3;
    }
    if (C == 'R')
    {
        cout << "L:";
        for(int i: L) cout << i << " ";
        cout << "\n";
        cout << "R:";
        for(int i: R) cout << i << " ";
        cout << "\n";
    }
    else
    {
        cout << "L:";
        for(int i: R) cout << i << " ";
        cout << "\n";
        cout << "R:";
        for(int i: L) cout << i << " ";
        cout << "\n";
    }
}
