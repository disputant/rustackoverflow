// https://informatics.msk.ru/moodle/mod/statements/view3.php?chapterid=112792#
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

vector<unsigned int> crazy(const vector<unsigned int>& a, const vector<unsigned int>& b)
{
    vector<unsigned int> c;
    for(size_t i = 0; i < b.size(); ++i)
    {
        unsigned int m = 0, aa = 0;
        for(size_t j = 0; j < a.size(); ++j)
        {
            unsigned int res = a[j]^b[i];
            if (res > m) { m = res; aa = a[j]; }
        }
        c.push_back(aa);
        //cout
        //    << dec << setw(10) << b[i] << " " << setw(10) << aa << "   "
        //    << hex << setw(8) << b[i] << " " << setw(8) << aa << endl;
    }
    return c;
}

struct node
{
    node():n0(0),n1(0){};
    node*n0,*n1;
};


node* make_trie(const vector<unsigned int>& a)
{
    node * root = new node;
    for(size_t j = 0; j < a.size(); ++j)
    {
        unsigned int n = a[j];
        node * r = root;
        for(int i = 31; i >= 0; --i)
        {
            unsigned int bit = (n>>i)&1;
            if (bit == 0)
            {
                if (r->n0 == 0) r->n0 = new node;
                r = r->n0;
            }
            else
            {
                if (r->n1 == 0) r->n1 = new node;
                r = r->n1;
            }
        }
    }
    return root;
}

vector<unsigned int> go(const vector<unsigned int>& b)
{
    vector<unsigned int> c;
    for(size_t j = 0; j < b.size(); ++j)
    {
        unsigned int m = b[j];
        node * r = root;
        unsigned int d = 0;
        for(int i = 31; i >= 0; --i)
        {

            d = d*2;
            unsigned int bit = 1 - (m>>i)&1;
            if (bit == 0)
            {
                r = (r->n0) ? r->n0 : (d++, r->n1);
            }
            else
            {
                r = (r->n1) ? (d++, r->n1) : r->n0;
            }
        }
        c.push_back(d);
    }
    return c;
}

int main()
{
    unsigned int n,m;
    cin >> n;
    vector<unsigned int> a(n);
    for(unsigned int i=0; i < n; ++i) cin >> a[i];
    cin >> m;
    vector<unsigned int> b(m);
    for(unsigned int i=0; i < m; ++i) cin >> b[i];
    make_trie(a);

    //cout << (crazy(a,b) == go(b)) << endl;

    for(auto i: go(b)) cout << i << " ";

}
