// https://ipc.susu.ru/28620.html
// Однопроходный вариант

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int n,m, sum = 0;
    cin >> n >> m;
    int * a = new int[m+2]{0};
    for(int i = 0; i <= n; ++i)
    {
        for(int j = 1; j <= m+1; ++j)
        {
            int x = 0;
            if (i < n && j <= m) cin >> x;

            if (x < (a[j]&~0x80) && (a[j]&0x80)) sum++;

            if ((a[j]&~0x80) < x) a[j] = x | 0x80; else a[j] = x;

            if (x <= (a[j-1]&~0x80)) a[j] &= ~0x80;
            if (x >= (a[j-1]&~0x80)) a[j-1] &= ~0x80;

        }
    }
    cout << sum;
}


#if 0

// Простейший вариант

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

int ok(const vector<vector<int>>& v, int i, int j)
{
    if (i-1 >= 0 && v[i-1][j] >= v[i][j]) return 0;
    if (i+1 < v.size() && v[i+1][j] >= v[i][j]) return 0;
    if (j-1 >= 0 && v[i][j-1] >= v[i][j]) return 0;
    if (j+1 < v[i].size() && v[i][j+1] >= v[i][j]) return 0;
    return 1;
}

int main(int argc, const char * argv[])
{
    int n, m, sum = 0;
    cin >> n >> m;
    vector<vector<int>> v(n,vector<int>(m));
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < m; ++j)
            cin >> v[i][j];

    for(int i = 0; i < n; ++i)
        for(int j = 0; j < m; ++j)
            sum += ok(v,i,j);
    cout << sum << endl;
}

#endif


#if 0

// Однопроходный с массивом пар

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int n,m, sum = 0;
    cin >> n >> m;
    pair<int,int> * a = new pair<int,int>[m+2]{{0,0}};
    for(int i = 0; i <= n; ++i)
    {
        for(int j = 1; j <= m+1; ++j)
        {
            int x = 0;
            if (i < n && j <= m) cin >> x;

            if (x < a[j].first && a[j].second) sum++;

            a[j].second = false;
            if (a[j].first < x) a[j].second = true;
            a[j].first = x;

            if (x <= a[j-1].first) a[j].second = false;
            if (x >= a[j-1].first) a[j-1].second = false;

        }
    }
    cout << sum;
}

#endif
