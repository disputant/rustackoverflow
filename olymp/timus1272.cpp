/*
 * Filename: test.cpp
 * Created:  Mon Sep 16 2019
 */

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[])
{
    int N, K, M;
    cin >> N >> K >> M;
    vector<int> v(N+1);
    for(int i = 0; i <= N; ++i) v[i] = i;

    for(int i = 0; i < K; ++i)
    {
        int a, b;
        cin >> a >> b;
        if (a > b) { int t = a; a = b; b = t; }

        if (v[a] == a && v[b] == b)  // ��� ���吝� ���設�
        {
            v[b] = a;  // ������ 㪠�뢠�� �� �������
        }
        else
        {
            while(v[b] < b) b = v[b];
            while(v[a] < a) a = v[a];
            if (a < b) v[b] = a; else v[a] = b;
        }
    }
    int count = 0;
    for(int i = 1; i <=N; ++i) if (v[i] == i) count++;

    cout << count-1 << endl;
}
