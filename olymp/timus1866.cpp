#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>

using namespace std;

string handle(string s)
{
    string t,r;
    replace(s.begin(),s.end(),'-',' ');
    istringstream in(s);
    while(in >> t)
    {
        if (t[0] == '[') r += '1'; else r+= '0';
    }
    return r;
}

bool check(string s, string m)
{
    string p = m;
    while(p.length() < s.length()) p += m;
    // cout << "check " << s << " for " << p << endl;
    for(int i = 0; i < s.length(); ++i)
        if (s[i] != p[i]) return false;
    return true;
}

string type(string s)
{
    if (check(s,"100")) return "dactyl";
    if (check(s,"010")) return "amphibrach";
    if (check(s,"001")) return "anapaest";
    if (check(s,"01")) return "iamb";
    if (check(s,"10")) return "trochee";
    return "not a poem";
}

bool poem(const vector<string>& v)
{
    vector<string> ans;
    for(int i = 0; i < v.size(); ++i)
        ans.push_back(type(v[i]));
    for(int i = 1; i < v.size(); ++i)
        if (ans[i] != ans[0]) return false;
    return true;
}



int main(int argc, const char * argv[])
{
    int N;
    (cin >> N).ignore(200,'\n');

    vector<string> v;
    for(int i = 0; i < N; ++i)
    {
        string s;
        getline(cin,s);
        s = handle(s);
        // cout << s << endl;
        v.push_back(s);
        // cout << "v.size = " << v.size() << endl;
    }
    // Проверка совпадения
    if (!poem(v)) { cout << "not a poem\n"; return 0; }
    cout << type(v[0]) << endl;
}
