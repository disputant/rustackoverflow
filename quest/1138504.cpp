#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

// https://ru.stackoverflow.com/a/1138504/195342

using namespace std;

template<typename T>
ostream& write(ostream& out, const vector<T>& d)
{
    size_t sz = d.size();
    out.write((char*)&sz,sizeof(sz));
    out.write((char*)d.data(),sz*sizeof(T));
    return out;
}

template<typename T>
istream& read(istream& is, vector<T>& d)
{
    d.clear();
    size_t sz;
    is.read((char*)&sz,sizeof(sz));
    d.resize(sz);
    is.read((char*)d.data(),sz*sizeof(T));
    return is;
}


template<typename T>
ostream& write(ostream& out, const vector<vector<T>>& d)
{
    size_t sz = d.size();
    out.write((char*)&sz,sizeof(sz));
    for(const auto& v:d)
    {
        write(out,v);
    }
    return out;
}

template<typename T>
istream& read(istream& is, vector<vector<T>>& d)
{
    d.clear();
    size_t sz;
    is.read((char*)&sz,sizeof(sz));
    d.resize(sz);
    for(auto& v:d)
    {
        read(is,v);
    }
    return is;
}


int main(int argc, const char * argv[])
{
    vector<vector<vector<double>>> z = {{{1,2,3},{9,8,7}},{{4,5,6},{0,5,9}}},
    t;

    ofstream os ("list.dat", ios::binary);
    write(os,z);
    os.close();

    ifstream is ("list.dat", ios::binary);
    read(is,t);
    is.close();

    cout << (z == t) << endl;

    for(auto x: z)
    {
        for(auto y: x)
        {
            for(auto q: y)
                cout << q << " ";
            cout << " | ";
        }
        cout << "\n";
    }

    cout << "\n\n";

    for(auto x: t)
    {
        for(auto y: x)
        {
            for(auto q: y)
                cout << q << " ";
            cout << " | ";
        }
        cout << "\n";
    }
    cout << "\n\n";
}
