// https://ru.stackoverflow.com/q/1144156/195342
// Перевод шестнадцатеричной цифры в бинарный вид
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;


constexpr const char * convert(char hx) {
    constexpr const char * hex[16] = {
        "0000","0001","0010","0011","0100","0101","0110","0111",
        "1000","1001","1010","1011", "1100","1101","1110","1111" };
    if (hx >= '0' && hx <= '9') return hex[hx-'0'];
    else if (hx >= 'A' && hx <= 'F') return hex[hx-'A'+10];
    else if (hx >= 'a' && hx <= 'f') return hex[hx-'a'+10];
    return nullptr;
}

constexpr const char * converts(char hx) {
    constexpr const char * hex[16] = {
        "0000","0001","0010","0011","0100","0101","0110","0111",
        "1000","1001","1010","1011", "1100","1101","1110","1111" };
    return hex[(hx|0x20)%87%48];
}

constexpr const char * convert_(char hx) {
    return "0000\0000001\0000010\0000011\0000100\0000101\0000110\0000111"
        "\0001000\0001001\0001010\0001011\0001100\0001101\0001110\0001111"
    + (hx|0x20)%87%48*5;
}


int main(int argc, const char * argv[])
{
    constexpr auto x0 = convert('0');
    constexpr auto x1 = convert('1');
    constexpr auto x2 = convert('2');
    constexpr auto x3 = convert('3');
    constexpr auto x4 = convert('4');
    constexpr auto x5 = convert('5');
    constexpr auto x6 = convert('6');
    constexpr auto x7 = convert('7');
    constexpr auto x8 = convert('8');
    constexpr auto x9 = convert('9');
    constexpr auto xa = convert('a');
    constexpr auto xb = convert('b');
    constexpr auto xc = convert('c');
    constexpr auto xd = convert('d');
    constexpr auto xe = convert('e');
    constexpr auto xf = convert('f');
    constexpr auto xA = convert('A');
    constexpr auto xB = convert('B');
    constexpr auto xC = convert('C');
    constexpr auto xD = convert('D');
    constexpr auto xE = convert('E');
    constexpr auto xF = convert('F');
    cout << x0 << "\n" << x1 << "\n" << x2 << "\n" << x3 << "\n" << x4 << "\n"
         << x5 << "\n" << x6 << "\n" << x7 << "\n" << x8 << "\n" << x9 << "\n"
         << xa << "\n" << xb << "\n" << xc << "\n" << xd << "\n" << xe << "\n"
         << xf << "\n" << xA << "\n" << xB << "\n" << xC << "\n" << xD << "\n"
         << xE << "\n" << xF << "\n";
}

