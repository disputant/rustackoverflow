/*
https://ru.stackoverflow.com/q/1144962/195342

Имеем на входе массив из положительных чисел, необходимо привести сумму 
массива к нулю. Для этого есть на входе две переменные. Первая переменная 
позволяет обнулить элемент массива, вторая - преобразует элемент массива 
со знаком "-". Необходим алгоритм поиска наименее затратного 
преобразования массива. Пример:

    int[] array = {1,6,3,2,0}, int x = 2, int y = 5

Оптимальное решение: 1-6+3+2=0, стоимость преобразования 5.

    int[] array = {2,2,2,2}, int x = 2, int y = 10

Оптимальное решение: 0+0+0+0, стоимость 8(2*4).

    int[] array = {1,5,3,2,0}, int x = 2, int y = 5

Оптимальное решение: 0-5+3+2, стоимость 7(2+5).
*/


#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <map>

using namespace std;

#if 0
int zeroing(int * a, int n, int sum = 0, int k = 0)
{
    static map<pair<int,int>,int> m;
    if (k == n)
    {
        return sum ? 5*n : 0;  // Заведомо большое значение, если не вышло
    }

    if (auto it = m.find(pair<int,int>(sum,k)); it != m.end())
    {
        return it->second;
    }

    int none = zeroing(a,n,sum-a[k],k+1);
    int sign = 5 + zeroing(a,n,sum+a[k],k+1);
    int zero = 2 + zeroing(a,n,sum,k+1);
    int res = min(none,min(sign,zero));
    m.insert(make_pair(pair<int,int>(sum,k),res));
    return res;
}
#endif
int zeroing(int * a, int n, int sum = 0, int k = 0)
{
    if (k == n) return sum ? 5*n : 0;
    int none = zeroing(a,n,sum-a[k],k+1);
    int sign = 5 + zeroing(a,n,sum+a[k],k+1);
    int zero = 2 + zeroing(a,n,sum,k+1);
    return min(none,min(sign,zero));
}


int main()
{
    int x[] = { 1,5,3,2,0 };
    cout << zeroing(x,size(x)) << endl;

}
