#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

vector<string> keys;

void make_keys()
{
    ofstream ks("keys");
    for(int i = 0; i < 100; ++i)
    {
        string s;
        for(int j = 0; j < 5; ++j)
            s += 'a' + rand()%26;
        keys.push_back(s);
        ks << s << "\n";
    }
}

string rstr()
{
    static int no = 0;
    int count = 50;
    bool add = false;
    string s;
    s.reserve(count);

    if (no++%10==0)
    {
        count -= 5;
        add = true;
    }
    for(int i = 0; i < count; ++i)
        s += 'a' + rand()%26;
    if (add) s += keys[no%keys.size()];
    return s;
}

int main()
{
    make_keys();

    ofstream os("data");
    for(int i = 0; i < 100000000; ++i)
    {
        os << rstr() << "\t" << rstr() << "\n";
    }
}

