#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

vector<string> make_keys()
{
    ifstream ks("keys");
    vector<string> res;
    string s;
    while(getline(ks,s))
    {
        res.push_back(s);
    }
    return res;
}

int main()
{
    srand(time(0));
    vector<string> keys = make_keys();

    ifstream in("data");
    ofstream out("found");

    string s;

    while(getline(in,s))
    {
        bool found = false;
        for(const auto& k: keys)
        {
            if (s.find(k) != s.npos)
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            auto pos = s.find_first_of(" \t");
            if (pos != s.npos) s = s.substr(0,pos);
            out << s << "\n";
        }
    }
}

