#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <chrono>

using namespace std;

class muTimer
{
    using Clock = std::chrono::high_resolution_clock;
    bool active = false;
    Clock::duration   duration_;
    Clock::time_point start_ = Clock::now(), stop_ = Clock::now();

    muTimer(const muTimer&)             = delete;
    muTimer& operator=(const muTimer&)  = delete;
public:
    using ns       = std::chrono::nanoseconds;
    using mks      = std::chrono::microseconds;
    using ms       = std::chrono::milliseconds;
    muTimer() { reset(); start(); }
    ~muTimer() = default;
    muTimer& reset()
    {
        duration_ = std::chrono::nanoseconds(0);
        active    = false;
        return *this;
    }
    muTimer& start()
    {
        if (!active)
        {
            start_ = Clock::now();
            active = true;
        }
        return *this;
    }
    muTimer& stop()
    {
        if (active)
        {
            stop_      = Clock::now();
            duration_ += stop_ - start_;
            active     = false;
        }
        return *this;
    }
    template<typename T = mks>
        unsigned long long duration()
    {
        return static_cast<unsigned long long>
            (std::chrono::duration_cast<T>(stop_-start_).count());
    }
};


#pragma warning(disable:4838)
#pragma warning(disable:4309)

char my_char1[] = {1,5,45,255,56,45,1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45,'\0'};
char my_char2[] = {1,5,45,255,56,45,1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45, 1,5,45,255,56,45,'\0'};

int main()
{

    {
        muTimer mt;
        int stat = 0;
        for (int y = 0; y < 100000000; y++)
        {
            bool eq = true;
            for (int i = 0; i < sizeof(my_char2); i++)
            {
                if (my_char1[i] != my_char2[i])
                {
                    eq = false;
                    break;
                }
            }
            if (eq) stat += 1;
        }
        mt.stop();
        cout << stat << endl;
        cout << mt.duration<>() << " mks" << endl;
    }
    {
        muTimer mt;
        int stat = 0;
        for (int y = 0; y < 100000000; y++)
        {
            bool eq = true;
            for (const int *a = (const int *)my_char1, *b = (const int *)my_char2;
                 (char*)a < my_char1 + sizeof(my_char1); a++, b++)
            {
                if (*a != *b)
                {
                    eq = false;
                    break;
                }
            }
            if (eq) stat += 1;
        }
        mt.stop();
        cout << stat << endl;
        cout << mt.duration<>() << " mks" << endl;
    }
    {
        muTimer mt;
        int stat = 0;
        for (int y = 0; y < 100000000; y++)
        {
            if (strncmp(my_char1, my_char2, sizeof(my_char2)) == 0)
            {
                stat += 1;
            }
        }
        mt.stop();
        cout << stat << endl;
        cout << mt.duration<>() << " mks" << endl;
    }
    {
        muTimer mt;
        int stat = 0;
        for (int y = 0; y < 100000000; y++)
        {
            if (strcmp(my_char1, my_char2) == 0)
            {
                stat += 1;
            }
        }
        mt.stop();
        cout << stat << endl;
        cout << mt.duration<>() << " mks" << endl;
    }
}
