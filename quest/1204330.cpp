#include <iostream>

using namespace std;

pair<unsigned long long, unsigned long long> fib(unsigned long long N,
                                                 unsigned long long A,
                                                 unsigned long long B,
                                                 unsigned long long C)
{
    unsigned long long a[2][2] = {{1,1},{1,0}};
    unsigned long long r[2][2] = {{1,0},{0,1}};
    while(N)
    {
        unsigned long long b[2][2];
        if (N&1) {
            b[0][0] = a[0][0]*r[0][0] + a[0][1]*r[1][0];
            b[0][1] = a[0][0]*r[0][1] + a[0][1]*r[1][1];
            b[1][0] = a[1][0]*r[0][0] + a[1][1]*r[1][0];
            b[1][1] = a[1][0]*r[0][1] + a[1][1]*r[1][1];
            r[0][0] = b[0][0]%C;
            r[0][1] = b[0][1]%C;
            r[1][0] = b[1][0]%C;
            r[1][1] = b[1][1]%C;
        }
        N >>= 1;
        b[0][0] = a[0][0]*a[0][0] + a[0][1]*a[1][0];
        b[0][1] = a[0][0]*a[0][1] + a[0][1]*a[1][1];
        b[1][0] = a[1][0]*a[0][0] + a[1][1]*a[1][0];
        b[1][1] = a[1][0]*a[0][1] + a[1][1]*a[1][1];
        a[0][0] = b[0][0]%C;
        a[0][1] = b[0][1]%C;
        a[1][0] = b[1][0]%C;
        a[1][1] = b[1][1]%C;
    }
    return make_pair(r[0][0]*B + r[0][1]*A,r[1][0]*B+r[1][1]*A);
}

int main()
{
    unsigned long long A, B, C, K;
    cin >> A >> B >> C >> K;
    if (K == 1) cout << A << "\n";
    else if (K == 2) cout << B << "\n";
    else if (A == 0 && B == 0) cout << B << "\n";
    else
    {
        auto p = fib(K-2,A,B,C);
        A = p.first%C;
        if (A == 0) A = C;
        cout << A << endl;
    }
}
