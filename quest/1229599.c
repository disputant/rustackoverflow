//https://ru.stackoverflow.com/q/1229599/195342

#define  _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>

typedef struct Item_
{
    unsigned int p, k;
} Item;

Item f[11];

void factors(unsigned int n)
{
    memset(f,0,sizeof(f));
    int idx = 0;
    while(n > 1 && n%2 == 0)
    {
        f[idx].p = 2;
        f[idx].k++;
        n /= 2;
    }
    if (f[idx].p) idx++;

    if (n > 1)
        for(unsigned int i = 3; i*i <= n; i += 2)
        {
            while(n%i == 0)
            {
                f[idx].p = i;
                f[idx].k++;
                n /= i;
            }
            if (f[idx].p) idx++;
        }
    if (n > 1)
    {
        f[idx].p = n;
        f[idx].k = 1;
    }

    for(int i = 0; f[i].p; ++i)
        printf("%u^%u ",f[i].p, f[i].k);
    puts("");
}

void outpairs(unsigned int N, unsigned int p, int m )
{
    if (f[m].p == 0)
    {
        printf("%u * %u = %u\n",p,N/p,p*(N/p));
        return;
    }
    unsigned int k = 1, i = 0;
    do {
        if (p*k > sqrt(N)) return;
        outpairs(N,p*k,m+1);
        k *= f[m].p;
    } while(++i <= f[m].k);
}

int main()
{
    unsigned int N;
    scanf("%u",&N);
    factors(N);
    outpairs(N,1,0);
}



