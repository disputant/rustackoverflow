// https://ru.stackoverflow.com/q/1284844/195342
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <stack>

using namespace std;

int k(string& s)
{
    int ks = 0;
    int pos = s.find('G');
    for(int p = pos; p >= 0 && s[p] != 'D'; --p)
        if (s[p] == 'K') { s[p] = ' '; ++ks; }
    for(int p = pos; p < s.size() && s[p] != 'D'; ++p)
        if (s[p] == 'K') { s[p] = ' '; ++ks; }
    return ks;
}


bool l(const string& s, int keys, stack<bool>&st);
bool r(const string& s, int keys, stack<bool>&st);

bool l(const string& s, int keys, stack<bool>&st)
{
    int pos = s.find('G');
    if (pos == 0 || pos == s.size()-1) return true;
    string t = s;
    keys += k(t);
    int p = pos-1;
    for(; p >= 0; --p)
    {
        if (t[p] == 'D')
        {
            if (keys > 0)
            {
                t[p] = ' ';
                keys--;
                t[p] = 'G';
                t[pos] = ' ';
                bool res = l(t,keys,st)||r(t,keys,st);
                if (res)
                {
                    st.push(false);
                    //cout << "L: keys = " << keys << " t = " << t << endl;
                }
                return res;
            }
            return false;
        }
    }
    return true;
}

bool r(const string& s, int keys, stack<bool>&st)
{
    int pos = s.find('G');
    if (pos == 0 || pos == s.size()-1) return true;
    string t = s;
    keys += k(t);
    int p = pos+1;
    for(; p < t.size(); ++p)
    {
        if (t[p] == 'D')
        {
            if (keys > 0)
            {
                t[p] = ' ';
                keys--;
                t[p] = 'G';
                t[pos] = ' ';
                bool res = l(t,keys,st)||r(t,keys,st);
                if (res)
                {
                    st.push(true);
                    //cout << "R: keys = " << keys << " t = " << t << endl;
                }
                return res;
            }
            return false;
        }
    }
    return true;
}

void out(string s,stack<bool> st)
{
    cout << s << endl;
    while(!st.empty())
    {
        k(s);
        int pos = s.find('G');
        bool d = st.top();
        st.pop();
        s[pos] = ' ';
        while(s[pos] != 'D') pos = pos + (d ? 1 : -1);
        s[pos] = 'G';
        cout << s << endl;
    }
}


int main(int argc, char * argv[])
{
    stack<bool> st;
    string s = "DDDDKKKDDKGDKKDDD";
    cout << (l(s,0,st)||r(s,0,st)) << endl;

    out(s,st);


    //s = "DKDDGKKDDKDD";
    //cout << (l(s,0)||r(s,0)) << endl;
    //s = "DDKGDDKKKKKKD";
    //cout << (l(s,0)||r(s,0)) << endl;
    //s = "DDDDKKKDDKGDKKDDD";
    //cout << (l(s,0)||r(s,0)) << endl;
    
}
