// https://ru.stackoverflow.com/q/1363674/195342
#include <vector>
#include <iostream>
#include <iomanip>
#include <tuple>
#include <queue>
#include <set>
#include <stack>

using namespace std;

class Vertex
{
    static constexpr int N = 8;
    int wr_, wc_;
    int br_, bc_;
    mutable const Vertex* prev = nullptr;
public:
    Vertex(int wr, int wc, int br, int bc):wr_(wr),wc_(wc),br_(br),bc_(bc){}

    bool operator == (const Vertex& v) const {
        return tie(wr_,wc_,br_,bc_) == tie(v.wr_,v.wc_,v.br_,v.bc_);
    }
    bool operator <(const Vertex& v) const {
        return tie(wr_,wc_,br_,bc_) < tie(v.wr_,v.wc_,v.br_,v.bc_);
    }

    void setPrev(const Vertex *p) const { prev = p; }
    const Vertex* getPrev() const { return prev; }

    vector<Vertex> moves() const;

    friend ostream& operator <<(ostream& out, const Vertex& v);

};

ostream& operator <<(ostream& out, const Vertex& v)
{
    return out << "[("  << v.wc_ << "," << v.wr_
               << "),(" << v.bc_ << "," << v.br_ << ")]";
}

vector<Vertex> Vertex::moves() const
{
    constexpr int dr[] = { -2, -2, -1, -1,  1,  1,  2,  2 };
    constexpr int dc[] = { -1,  1, -2,  2, -2,  2, -1,  1 };
    vector<Vertex> res;
    // White moves
    for(int i = 0; i < 8; ++i)
    {
        int wrn = wr_ + dr[i], wcn = wc_ + dc[i];
        if (wrn < 0 || wrn >= N || wcn < 0 || wcn >= N) continue;
        if (wrn == br_ && wcn == bc_) continue;
        // Black moves
        for(int j = 0; j < 8; ++j)
        {
            int brn = br_ + dr[j], bcn = bc_ + dc[j];
            if (brn < 0 || brn >= N || bcn < 0 || bcn >= N) continue;
            if (brn == wrn && bcn == wcn) continue;

            Vertex w(wrn,wcn,brn,bcn);
            res.push_back(w);
        }
    }
    return res;
}

int main()
{
    int wr = 2, wc = 2, br = 4, bc = 5;

    set<Vertex> seen;
    queue<Vertex> Q;
    Vertex start(wr,wc,br,bc);
    Vertex stop(br,bc,wr,wc);
    Q.push(start);
    seen.insert(start);

    while(!Q.empty())
    {
        const Vertex& v = *seen.find(Q.front());
        Q.pop();

        // cout << "Get " << v << "from queue, size = " << Q.size() << "\n";

        if (v == stop) { stop = v; break; }
        for(Vertex& w: v.moves())
        {
            if (auto [it, ok] = seen.insert(w); ok)
            {
                it->setPrev(&v);
                Q.push(w);
            }
        }
    }

    stack<Vertex> st;
    for(const Vertex * v = &stop; v; v = v->getPrev())
    {
        st.push(*v);
        // cout << "Push " << *v << "into stack\n";
    }
    while(!st.empty())
    {
        cout << st.top() << " ";
        st.pop();
    }

}
