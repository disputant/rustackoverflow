//https://ru.stackoverflow.com/q/1538768/195342

#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char * argv[])
{
    int n, m;
    long long best = 0;
    cin >> n >> m;
    vector<long long> t[8];
    for(int i = 0; i < n; ++i)
    {
        int a, b, c;
        cin >> a >> b >> c;
        t[0].push_back( a + b + c);
        t[1].push_back( a + b - c);
        t[2].push_back( a - b + c);
        t[3].push_back( a - b - c);
        t[4].push_back(-a + b + c);
        t[5].push_back(-a + b - c);
        t[6].push_back(-a - b + c);
        t[7].push_back(-a - b - c);
    }
    for(int i = 0; i < 8; ++i)
    {
        sort(t[i].begin(),t[i].end(),greater<long long>());
        long long s = 0;
        for(int j = 0; j < m; ++j) s += t[i][j];
        if (best < s) best = s;
    }
    cout << best;
}


