/*
 * Filename: abm.cpp
 * Created:  Fri Nov 25 2016
 * https://ru.stackoverflow.com/q/595934/195342
 */

#include <cstdlib>
#include <cstdio>
#include <climits>

using namespace std;

const int N = 15;
int A[N][N];
int B[N][N];

inline int get(int X[N][N], int r, int c)
{
    if (r<0 || r>=N || c<0 || c >=N) return INT_MIN;
    return X[r][c];
}


int main(int argc, const char * argv[])
{

    for(int i = 0; i < N; ++i)
    {
        for(int j = 0; j < N; ++j)
        {
            A[i][j] = rand()%100;
            printf("%4d",A[i][j]);
        }
        printf("\n");
    }
    printf("\n\n");

    for(int c = 0; c < N; ++c)
    {
        for(int r = 0; r < N; ++r)
        {
            int v0 = get(B,r-1,c-1);
            int v1 = get(B,r,  c-1);
            int v2 = get(B,r+1,c-1);
            int v3 = get(A,r  ,c  );
            v0 = (v0 > v1) ? v0 : v1;
            v2 = (v2 > v3) ? v2 : v3;
            B[r][c] = (v0 > v2) ? v0 : v2;
        }
    }

    for(int i = 0; i < N; ++i)
    {
        for(int j = 0; j < N; ++j)
        {
            printf("%4d",B[i][j]);
        }
        printf("\n");
    }
    printf("\n\n");

}

