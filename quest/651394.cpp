/*
 * Filename: past.cpp
 * Created:  Sun Apr 09 2017
 * https://ru.stackoverflow.com/q/651394/195342

������ ����� �� ������ �������� ������ ������ ������ ����� � ����� ����� ����������� ����. �� ������, ��� ����� ������ ������ ����� �������� ���� � ������ �������. ����, ��� ������� �����, ����� ����������� ���������������, ����������� �� ���������� ������� �� �������� 1 ����. ���� ������� ��� ��������� �� ������ ��� �� ��� ����������� �����, �� ������� ����� �� ������ ���������. ����� ��������� �� ����� ����� ������ �������, � ����� �� ����� ������ ������� �������. ����� ����� ���������� � ������ ������� �� ����� �������� �������, ���� �� �� ����� �������. � �� ������� ������ ����� ������ ��������� ���� ������ � ������ �������?

������� ������

� ������ ������ ������ ��� ����������� ����� N � M (1 <= N, M < 500) � ������� ���� (���������� ����� � �������� �������� ����). � ��������� N ������� ������ ����� ���� ������ M ����� 0 ��� 1: 0 ��������, ��� ��������������� ������� ��������, 1 ��������, ��� �� ������� ��������� �����.

�������� ������

���� ����� � ����� ����������� ���� ������ � ������ �������.

������ ������� ������

6 6
1 1 0 0 1 0
1 0 0 1 1 0
0 1 0 0 0 1
1 1 0 1 1 1
1 1 1 0 1 1
0 0 0 0 1 1

������ �������� ������

8

 */

#include <vector>
#include <iostream>
#include <iomanip>
#include <limits>
#include <queue>

using namespace std;

struct vertex
{
    unsigned int r, c;
    int value = 0;
    int color = 0; // white; 1 - gray, 2 - black
    int dist  = numeric_limits<int>::max();
};

vector<vector<vertex>> mx;

int main()
{
    unsigned int N, M;
    cin >> N >> M;
    mx = vector<vector<vertex>>(N,vector<vertex>(M));
    for(unsigned int r = 0; r < N; ++r)
        for(unsigned int c = 0; c < M; ++c)
        {
            mx[r][c].r = r;
            mx[r][c].c = c;
            cin >> mx[r][c].value;
        }
    mx[N-1][0].dist  = 0;
    mx[N-1][0].color = 1;
    // begin: (N-1,0) end: (0,N-1)

    queue<vertex*> Q;
    Q.push(&mx[N-1][0]);
    while(!Q.empty())
    {
        vertex* u = Q.front();
        Q.pop();
        if (u->r == 0 && u->c == M-1)
        {
            cout << u->dist << endl;
            return 0;
        }
        for(int dr = -1; dr <= 1; ++dr)
        {
            int r = u->r + dr;
            if (r < 0 || r >= N) continue;
            for(int dc = -1; dc <= 1; ++dc)
            {
                if (dr == 0 && dc == 0) continue;
                int c = u->c + dc;
                if (c < 0 || c >= M) continue;
                if (mx[r][c].value == 1) continue;
                if (mx[r][c].color != 0) continue;

                mx[r][c].color = 1;
                mx[r][c].dist = u->dist + 1;
                Q.push(&mx[r][c]);

            }
        }
        u->color = 2;
    }
}

