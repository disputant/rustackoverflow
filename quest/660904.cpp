/*
 * Filename: test.cpp
 * Created:  Tue May 02 2017
 * https://ru.stackoverflow.com/q/660904/195342

�� �室 �ணࠬ�� ������ ��� �窨: �窠 ���� � �窠 䨭��. ����室��� �� �窨 ���� ������� � ��� 䨭��. ������ �� �� �窨 ����� ��᪮�쪨�� ᯮᮡ���.

int start; //�窠 ���� 
2*start; //�� �� �窨
(3*start)+1; //⮦� �� �� �窨
start/2; //⮫쪮 �� �祪 � ��� ����஬
(start-1)/3; //�� �祪 ����� ������ �� ������� �� 3 ���� ���⮪ �� ������� 1
��� ᤥ���� ��ॡ�� ��� ��ਠ�⮢ � �뢥�� ᠬ� ���⪨� ����? ������� ᠬ ������.

�ਬ�� �ணࠬ��.

�� �室 1 6.

�� ��室

7

1 4 8 16 5 10 3 6

 */

#include <vector>
#include <iostream>
#include <iomanip>
#include <queue>
#include <stack>
#include <set>

using namespace std;

struct Node
{
    Node(int i, int p = -1):idx(i),prev(p){}
    int idx;   // ������
    int prev;  // �।��騩 㧥� - ��� ����⠭������� ���
    bool operator <(const Node& n) const { return idx < n.idx; }
};

int main()
{
    int start = 1, stop = 6;

    queue<Node> Q;   // ��।� BFS
    set<Node> S;     // ������⢮ 㦥 ��ࠡ�⠭��� 㧫��

    Q.push(Node(start));
    while(!Q.empty())
    {
        S.insert(Q.front());        // ���᫨ � ��ࠡ�⠭��
        int index = Q.front().idx;  // ������
        if (index == stop) break;   // ������!
        Q.pop();                    // ���ࠥ� �� ��।�
        vector<int> next;           // �������� ��ਠ���
        next.push_back(2*index);
        next.push_back(3*index+1);
        if (index%2 == 0) next.push_back(index/2);
        if (index%3 == 1) next.push_back((index-1)/3);
        for(int i: next)            // ��ࠡ�⪠ ��������� ��ਠ�⮢
        {
            Node n(i,index);
            if (S.find(n) != S.end()) continue;  // ��� ��ࠡ�⠭
            Q.push(n);              // ���ᥭ�� � ��।� �� �� ��ᬮ�७���
        }
    }

    // �뢮� 楯�窨 - ⠪ ��� � ���⭮� ���浪�, �ᯮ��㥬 �⥪
    stack<int> path;
    for(int index = Q.front().idx; index != -1; )
    {
        path.push(index);
        auto i = S.find(Node(index));    // ���� �।��饣�
        if (i == S.end()) break;         // ��०����� ��� ��०�� :)
        index = i->prev;                 // �।��騩 � 楯�窥
    }
    // �뢮� �� �⥪�
    while(!path.empty())
    {
        cout << path.top() << "  ";
        path.pop();
    }
    cout << endl;
}

